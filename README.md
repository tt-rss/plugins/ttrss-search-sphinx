# Installation

Checkout to `plugins.local/search_sphinx/`.

Add required constants to `config.php`:

```php
	// *********************
	// *** Sphinx search ***
	// *********************
	define('SPHINX_SERVER', 'localhost:9312');

	define('SPHINX_ENABLED', false);
	// Enable fulltext search using Sphinx (http://www.sphinxsearch.com)
	// Please see http://tt-rss.org/wiki/SphinxSearch for more information.

	define('SPHINX_INDEX', 'ttrss');
	// Index name in Sphinx configuration. You can specify multiple indexes
	// as a comma-separated string.
```

